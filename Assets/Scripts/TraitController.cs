﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class TraitController : MonoBehaviour {

    public GameObject[] combatant = new GameObject[2];

    public int blacks, browns, whites, girls, beards;

    public Button white1, brown1, black1, hair1, beard1, white2, brown2, black2, hair2, beard2;  

	// Use this for initialization
	void Start () {
        white1.onClick.AddListener(make1White);
        brown1.onClick.AddListener(make1Brown);
        black1.onClick.AddListener(make1Black);
        hair1.onClick.AddListener(make1girl);
        beard1.onClick.AddListener(make1bearded);

        white2.onClick.AddListener(make2White);
        brown2.onClick.AddListener(make2Brown);
        black2.onClick.AddListener(make2Black);
        hair2.onClick.AddListener(make2girl);
        beard2.onClick.AddListener(make2bearded); 
    }

    // Update is called once per frame
    private void OnGUI()
    {
        updateCount(); 
    }

    public void make1Black()
    {
        combatant[0].transform.GetChild(0).GetComponent<Image>().color = new Color32(91, 1, 1, 255);
        combatant[0].GetComponent<Profile>().skin = "black"; 
    }
    public void make1White()
    {
        combatant[0].transform.GetChild(0).GetComponent<Image>().color = Color.white;
        combatant[0].GetComponent<Profile>().skin = "white"; 
    }
    public void make1Brown()
    {
        combatant[0].transform.GetChild(0).GetComponent<Image>().color = new Color32(165, 42, 42, 255);
        combatant[0].GetComponent<Profile>().skin = "brown"; 
    }

    public void make2Black()
    {
        combatant[1].transform.GetChild(0).GetComponent<Image>().color = new Color32(91, 1, 1, 255);
        combatant[1].GetComponent<Profile>().skin = "black";
    }
    public void make2White()
    {
        combatant[1].transform.GetChild(0).GetComponent<Image>().color = Color.white;
        combatant[1].GetComponent<Profile>().skin = "white";
    }
    public void make2Brown()
    {
        combatant[1].transform.GetChild(0).GetComponent<Image>().color = new Color32(165, 42, 42, 255);
        combatant[1].GetComponent<Profile>().skin = "brown";
    }

    public void make1bearded()
    {
        if (combatant[0].GetComponent<Profile>().hair == "bearded")
        {
            combatant[0].transform.GetChild(2).gameObject.SetActive(false);
            combatant[0].transform.GetChild(3).gameObject.SetActive(false);
            combatant[0].GetComponent<Profile>().hair = "";
        }
        else
        {
            combatant[0].transform.GetChild(3).gameObject.SetActive(false);
            combatant[0].transform.GetChild(2).gameObject.SetActive(true);
            combatant[0].GetComponent<Profile>().hair = "bearded";
        }
    }

    public void make1girl()
    {
        if (combatant[0].GetComponent<Profile>().hair == "girl")
        {
            combatant[0].transform.GetChild(2).gameObject.SetActive(false);
            combatant[0].transform.GetChild(3).gameObject.SetActive(false);
            combatant[0].GetComponent<Profile>().hair = "";
        }
        else
        {
            combatant[0].transform.GetChild(2).gameObject.SetActive(false);
            combatant[0].transform.GetChild(3).gameObject.SetActive(true);
            combatant[0].GetComponent<Profile>().hair = "girl";
        }
    }

    public void make2bearded()
    {
        if (combatant[1].GetComponent<Profile>().hair == "bearded") {
            combatant[1].transform.GetChild(2).gameObject.SetActive(false);
            combatant[1].transform.GetChild(3).gameObject.SetActive(false);
            combatant[1].GetComponent<Profile>().hair = "";
        } else
        {
            combatant[1].transform.GetChild(3).gameObject.SetActive(false);
            combatant[1].transform.GetChild(2).gameObject.SetActive(true);
            combatant[1].GetComponent<Profile>().hair = "bearded";

        }
    }

    public void make2girl()
    {
        if (combatant[1].GetComponent<Profile>().hair == "girl") {
            combatant[1].transform.GetChild(2).gameObject.SetActive(false);
            combatant[1].transform.GetChild(3).gameObject.SetActive(false);
            combatant[1].GetComponent<Profile>().hair = "";
        } else
        {
            combatant[1].transform.GetChild(2).gameObject.SetActive(false);
            combatant[1].transform.GetChild(3).gameObject.SetActive(true);
            combatant[1].GetComponent<Profile>().hair = "girl";
        }
    }

    public void updateCount()
    {
        blacks = 0;
        browns = 0;
        whites = 0;
        girls = 0;
        beards = 0; 

        foreach (GameObject c in combatant)
        {
            string itsSkin = c.GetComponent<Profile>().skin;
            string itsHair = c.GetComponent<Profile>().hair; 
            switch (itsSkin)
            {
                case "black":
                    blacks++;
                    break;
                case "brown":
                    browns++;
                    break;
                case "white":
                    whites++;
                    break;
            }
            switch (itsHair)
            {
                case "girl":
                    girls++;
                    break;
                case "bearded":
                    beards++;
                    break;
            }
        }
    }
}
