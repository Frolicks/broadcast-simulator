﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HeadlineUpdater : MonoBehaviour
{

    private Text headline;
    private TraitController tc;

    public float girlTime;
    public bool erect;

    private string skin0, skin1, hair0, hair1;

    // Use this for initialization
    void Start()
    {
        headline = GameObject.Find("Headline").GetComponent<Text>();
        tc = GameObject.Find("GameController").GetComponent<TraitController>();
        girlTime = -5f;
        erect = true;
    }

    private void Update()
    {
        if (Time.time > girlTime && Time.time < girlTime + 5f)
        {
            headline.text = "ATTRACTIVE WOMEN FIGHT TO THE DEATH FOR CHANCE TO MATE WITH LOCAL GAME DEVELOPER";
        }
    }

    // Update is called once per frame
    private void OnGUI()
    {
        skin0 = tc.combatant[0].GetComponent<Profile>().skin;
        skin1 = tc.combatant[1].GetComponent<Profile>().skin;
        hair0 = tc.combatant[0].GetComponent<Profile>().hair;
        hair1 = tc.combatant[1].GetComponent<Profile>().hair;
        if (tc.girls == 1)
        {
            if (hair0 == "girl" && skin1 == "black" || hair1 == "girl" && skin0 == "black")
                headline.text = "THUG ";
            else if (hair0 == "girl" && skin1 == "brown" || hair1 == "girl" && skin0 == "brown")
                headline.text = "ALLEGED IMMIGRANT ";
            else
                headline.text = "MAN ";
            headline.text += "ASSAULTS LOCAL WOMAN";
        }
        else if (tc.beards == 2)
        {
            headline.text = "UNEMPLOYED ";
            if (tc.browns == 2)
                headline.text += "IMMIGRANTS ";
            else if (tc.blacks == 2)
                headline.text += "HOMELESS ";
            else
                headline.text += "LOCALS ";
            headline.text += "TURN VIOLENT";
        }
        else if (tc.browns == 1)
        {
            headline.text = "LOCAL ATTACKED BY ALLEGED IMMIGRANT";
        }
        else if (tc.blacks == 1)
        {
            headline.text = "LOCAL MUGGED IN BROAD DAYLIGHT";
        }
        else if (tc.blacks == 2)
        {
            headline.text = "GHETTOES ENGULFED IN GANG VIOLENCE";
            erect = false;
        }
        else if (tc.browns == 2)
        {
            headline.text = "IMMIGRANTS RIOT";
        }
        else
        {
            headline.text = "SCUFFLE TAKES PLACE DOWNTOWN";
            if (tc.girls == 2 && !erect)
            {
                girlTime = Time.time + 10f;
                erect = true;
            }
        }
        
    }
}